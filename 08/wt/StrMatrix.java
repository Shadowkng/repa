public class StrMatrix {
    public static void main(String[] args) {
        
        int[][] matrix = {
            {4, 3, 2, 1},
            {3, 2, 1, 4},
            {1, 2, 3, 4},
            {4, 1, 2, 3}};
       
        int l = matrix.length;
        for (int i = 0; i < l; i++) {
            for (int j = 0; j < l; j++) {
                System.out.print(matrix[i][j] + " ");
            }
        }
    }
}

